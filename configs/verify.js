module.exports = {
  isLoginUser(req, res, next) {
    if (req.session.loggedin === true && req.session.isadmin !== 1) {
      next();
      return;
    } else {
      req.session.destroy(function (err) {
        res.redirect("/");
      });
    }
  },
  isLoginAdmin(req, res, next) {
    if (req.session.loggedin === true && req.session.isadmin === 1) {
      next();
      return;
    } else {
      req.session.destroy(function (err) {
        res.redirect("/");
      });
    }
  },
  isLogoutUsers(req, res, next) {
    if (req.session.loggedin !== true && req.session.isadmin !== 1) {
      next();
      return;
    }
    res.redirect("/user");
  },
  isLogoutAdmin(req, res, next) {
    if (req.session.loggedin !== true && req.session.isadmin === 1) {
      next();
      return;
    }
    res.redirect("/admin");
  },
};
