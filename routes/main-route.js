const router = require("express").Router();
const loginController = require("../controllers").main;
const verifyUser = require("../configs/verify");

router.get("/", verifyUser.isLogoutUsers, loginController.main);
router.get("/", verifyUser.isLogoutAdmin, loginController.main);
router.get("/logout", loginController.logout);
router.post("/login", loginController.userAuth);
router.post("/login-admin", loginController.adminAuth);
router.post("/save", loginController.saveRegister);

module.exports = router;
