const router = require("express").Router();
const userController = require("../controllers").user;
const verifyUser = require("../configs/verify");

router.get("/", verifyUser.isLoginUser, userController.user);

module.exports = router;
