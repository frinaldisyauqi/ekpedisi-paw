const router = require("express").Router();
const adminController = require("../controllers").admin;
const verifyUser = require("../configs/verify");

router.get("/", verifyUser.isLoginAdmin, adminController.admin);

module.exports = router;
