const config = require("../configs/database");

let mysql = require("mysql");
let pool = mysql.createPool(config);

pool.on("error", (err) => {
  console.error(err);
});

module.exports = {
  user(req, res) {
    res.render("user/index", {
      url: "http://localhost:3000/",
      title: "J2F Express - User",
      userName: req.session.username,
    });
  },
};
