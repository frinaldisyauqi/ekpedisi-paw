const config = require("../configs/database");

let mysql = require("mysql");
let pool = mysql.createPool(config);

pool.on("error", (err) => {
  console.error(err);
});

module.exports = {
  main(req, res) {
    res.render("index", {
      url: "http://localhost:3000/",
      title: "J2F Express",
      colorFlash: req.flash("color"),
      statusFlash: req.flash("status"),
      pesanFlash: req.flash("message"),
    });
  },
  userAuth(req, res) {
    let email = req.body.email;
    let password = req.body.pass;
    if (email && password) {
      pool.getConnection(function (err, connection) {
        if (err) throw err;
        connection.query(
          `SELECT * FROM table_user WHERE user_email = ? AND user_password = ?`,
          [email, password],
          function (error, results) {
            if (error) throw error;
            if (results.length > 0) {
              req.session.loggedin = true;
              req.session.userid = results[0].user_id;
              req.session.username = results[0].user_name;
              req.session.isadmin = results[0].user_isadmin;
              res.redirect("/user");
            } else {
              req.flash("color", "danger");
              req.flash("status", "Oops..");
              req.flash("message", "Akun tidak ditemukan");
              res.redirect("/");
            }
          }
        );
        connection.release();
      });
    } else {
      res.redirect("/");
      res.end();
    }
  },
  adminAuth(req, res) {
    let email = req.body.email;
    let password = req.body.pass;
    let kode = req.body.cabangKode;
    if (email && password && kode) {
      pool.getConnection(function (err, connection) {
        if (err) throw err;
        connection.query(
          `SELECT * FROM table_user WHERE user_email = ? AND user_password = ? AND user_kode = ?`,
          [email, password, kode],
          function (error, results) {
            if (error) throw error;
            if (results.length > 0) {
              req.session.loggedin = true;
              req.session.userid = results[0].user_id;
              req.session.username = results[0].user_name;
              req.session.isadmin = results[0].user_isadmin;
              res.redirect("/admin");
            } else {
              req.flash("color", "danger");
              req.flash("status", "Oops..");
              req.flash("message", "Akun tidak ditemukan");
              res.redirect("/");
            }
          }
        );
        connection.release();
      });
    } else {
      res.redirect("/");
      res.end();
    }
  },
  logout(req, res) {
    req.session.destroy((err) => {
      if (err) {
        return console.log(err);
      }
      res.clearCookie("secretname");
      res.redirect("/");
    });
  },
  saveRegister(req, res) {
    let username = req.body.username;
    let email = req.body.email;
    let password = req.body.pass;
    if (username && email && password) {
      pool.getConnection(function (err, connection) {
        if (err) throw err;
        connection.query(
          `INSERT INTO table_user (user_name,user_email,user_password,user_isadmin) VALUES (?,?,?,0);`,
          [username, email, password],
          function (error, results) {
            if (error) throw error;
            req.flash("color", "success");
            req.flash("status", "Yes..");
            req.flash("message", "Registrasi berhasil");
            res.redirect("/");
          }
        );
        connection.release();
      });
    } else {
      res.redirect("/");
      res.end();
    }
  },
};
