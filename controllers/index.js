const main = require("./controller-main");
const user = require("./controller-user");
const admin = require("./controller-admin");

module.exports = {
  main,
  user,
  admin,
};
