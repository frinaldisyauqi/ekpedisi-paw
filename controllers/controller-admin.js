const config = require("../configs/database");

let mysql = require("mysql");
let pool = mysql.createPool(config);

pool.on("error", (err) => {
  console.error(err);
});

module.exports = {
  admin(req, res) {
    res.render("admin/index", {
      url: "http://localhost:3000/",
      title: "J2F Express - Admin",
      userName: req.session.username,
    });
  },
};
